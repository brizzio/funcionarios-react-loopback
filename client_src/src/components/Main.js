import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Diretorio from './Diretorio';
import About from './About';


const Main = ()=>(
    <main>
        <Switch>
            <Route exact path='/' component={Diretorio} />
            <Route exact path='/Sobre' component={About} />
        </Switch>
    </main>

)

export default Main;